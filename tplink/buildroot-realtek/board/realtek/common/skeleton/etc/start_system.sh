#!/bin/sh


CheckProcess()  
{  
    if [ "$1" = "" ];  
    then  
	    return 1  
    fi  
  
    PROCESS_NUM=`ps -ef | grep "$1" | grep -v "grep" | wc -l`  
    if [ $PROCESS_NUM -eq 1 ];  
    then  
        return 0  
    else  
        return 1  
    fi      
}  

CheckFiles()
{
	files=" /tplink/image/usrImage/app/sbin/core
			/tplink/image/usrImage/app/sbin/monitor.cfg
			/tplink/image/usrImage/app/sbin/scm
			/lib/modules/ethdriver.ko"

	for file in $files
	do
		if [ ! -f "$file" ];then
			return 1
		fi
	done
	
	return 0
}

InitCore()
{
	exepath="/tplink/exe"	
	if [ ! -d "$exepath" ];then
	   mkdir $exepath
	fi
	
	exepath="/tplink/image"	
	if [ ! -d "$exepath" ];then
	   mkdir $exepath
	fi
	
	exepath="/tplink/usrapp"	
	if [ ! -d "$exepath" ];then
	   mkdir $exepath
	fi
	

	AREA=`cat /proc/mtd | grep "usrimg2@main" | awk '{print $1}'`
	if [ "$AREA" = "mtd4:" ];
        then
          mount -t squashfs /dev/mtdblock4 /tplink/image
        else
          mount -t squashfs /dev/mtdblock3 /tplink/image
        fi
        
	CheckFiles
	CHKFLAG=$?
	if [ $CHKFLAG -eq 1 ];
	then
	    echo "retry mount!"
	    umount /tplink/image
	    if [ "$AREA" = "mtd4:" ];
	    then
	      mount -t squashfs /dev/mtdblock4 /tplink/image
	    else
	      mount -t squashfs /dev/mtdblock3 /tplink/image
	    fi
	    
		CheckFiles
		CHKFLAG=$?
	    if [ $CHKFLAG -eq 1 ];
	    then
	    	umount /tplink/image
	    echo "check area fail ,mount anther area!"
            if [ "$AREA" = "mtd4:" ];
            then
              mount -t squashfs /dev/mtdblock3 /tplink/image
            else
              mount -t squashfs /dev/mtdblock4 /tplink/image
            fi
	    fi
    fi	

	mount -t jffs2 /dev/mtdblock5 /tplink/usrapp
	
	mkdir /exe
	mount -t ramfs none /exe
	cp -rf /tplink/image/usrImage/* /exe
	ln -sf /exe/* /tplink/exe		
	

	
	mknod /dev/tp-eth c 127 0
		
	export LD_LIBRARY_PATH=/tplink/exe/lib
	export PATH="$PATH:/tplink/exe/app/sbin"
	
	if [ ! -f "/etc/fw_env.config" ];then
		echo "/dev/mtd1 0x0000 0x20000 0x10000" > /etc/fw_env.config
	fi
	
	chmod 777 /usr/bin/fw_printenv
	ln -s /usr/bin/fw_printenv /usr/bin/fw_setenv
}
   
while [ 1 ] ; do  
    InitCore	
    CheckProcess "monitor"  
    CheckQQ_RET=$?  
    if [ $CheckQQ_RET -eq 1 ];  
    then  
     killall -9 monitor 1> /dev/nul 2>&1
     cd /etc
     ./monitor  
    fi  
    sleep 2  
done  



