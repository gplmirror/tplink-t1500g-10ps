# compatible vars/defs
tplink-generic-package = $(generic-package)
tplink-autotools-package = $(autotools-package)
tplink-cmake-package = $(cmake-package)

# build tplink pkgs in $(BUILD_DIR)/tplink
BUILD_TPLINK_DIR := $(BUILD_DIR)/tplink

# strip vars
BR2_TPLINK_SOURCE_DIR_STRIP	:= $(call qstrip,$(BR2_TPLINK_SOURCE_DIR))
BR2_TPLINK_PROJECT_NAME_STRIP	:= $(call qstrip,$(BR2_TPLINK_PROJECT_NAME))
BR2_TPLINK_CFLAGS_STRIP			:= $(call qstrip,$(BR2_TPLINK_CFLAGS))
BR2_TPLINK_CFLAGS					:= $(BR2_TPLINK_CFLAGS_STRIP)

# record and redefine $(BUILD_DIR)
BUILD_DIR_BAK := $(BUILD_DIR)
BUILD_DIR := $(BUILD_TPLINK_DIR)

# include subdir mk files
-include tplink/*/*.mk

# restore $(BUILD_DIR)
BUILD_DIR := $(BUILD_DIR_BAK)
BUILD_DIR_BAK :=

.PHONY: tp-clean tp-clean-dirs
# filter more dirs use sed -r '/sdk|moduleA|moduleB/d'
tp-clean:
	find $(BUILD_DIR)/tplink -maxdepth 1 -mindepth 1 -type d | sed '/sdk/d' | xargs rm -rf

tp-clean-dirs:
	@find $(BUILD_DIR)/tplink -maxdepth 1 -mindepth 1 -type d | sed '/sdk/d' | sort

# to recover missing target/tplink
tp-recover:
	cp -rf $(TARGET_DIR)/../usrfs/exe/* $(TARGET_DIR)/tplink/


