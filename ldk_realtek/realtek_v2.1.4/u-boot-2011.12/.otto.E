# 1 "include/asm/arch/soc.h"
# 1 "/home/maple.chen/sdk-integrate-8390-8380/u-boot-2011.12//"
# 1 "<built-in>"
#define __STDC_HOSTED__ 0
#define __GNUC__ 3
#define __GNUC_MINOR__ 4
#define __GNUC_PATCHLEVEL__ 4
#define __SIZE_TYPE__ unsigned int
#define __PTRDIFF_TYPE__ int
#define __WCHAR_TYPE__ int
#define __WINT_TYPE__ unsigned int
#define __GXX_ABI_VERSION 1002
#define __USING_SJLJ_EXCEPTIONS__ 1
#define __SCHAR_MAX__ 127
#define __SHRT_MAX__ 32767
#define __INT_MAX__ 2147483647
#define __LONG_MAX__ 2147483647L
#define __LONG_LONG_MAX__ 9223372036854775807LL
#define __WCHAR_MAX__ 2147483647
#define __CHAR_BIT__ 8
#define __FLT_EVAL_METHOD__ 0
#define __FLT_RADIX__ 2
#define __FLT_MANT_DIG__ 24
#define __FLT_DIG__ 6
#define __FLT_MIN_EXP__ (-125)
#define __FLT_MIN_10_EXP__ (-37)
#define __FLT_MAX_EXP__ 128
#define __FLT_MAX_10_EXP__ 38
#define __FLT_MAX__ 3.40282347e+38F
#define __FLT_MIN__ 1.17549435e-38F
#define __FLT_EPSILON__ 1.19209290e-7F
#define __FLT_DENORM_MIN__ 1.40129846e-45F
#define __FLT_HAS_INFINITY__ 1
#define __FLT_HAS_QUIET_NAN__ 1
#define __DBL_MANT_DIG__ 53
#define __DBL_DIG__ 15
#define __DBL_MIN_EXP__ (-1021)
#define __DBL_MIN_10_EXP__ (-307)
#define __DBL_MAX_EXP__ 1024
#define __DBL_MAX_10_EXP__ 308
#define __DBL_MAX__ 1.7976931348623157e+308
#define __DBL_MIN__ 2.2250738585072014e-308
#define __DBL_EPSILON__ 2.2204460492503131e-16
#define __DBL_DENORM_MIN__ 4.9406564584124654e-324
#define __DBL_HAS_INFINITY__ 1
#define __DBL_HAS_QUIET_NAN__ 1
#define __LDBL_MANT_DIG__ 53
#define __LDBL_DIG__ 15
#define __LDBL_MIN_EXP__ (-1021)
#define __LDBL_MIN_10_EXP__ (-307)
#define __LDBL_MAX_EXP__ 1024
#define __LDBL_MAX_10_EXP__ 308
#define __DECIMAL_DIG__ 17
#define __LDBL_MAX__ 1.7976931348623157e+308L
#define __LDBL_MIN__ 2.2250738585072014e-308L
#define __LDBL_EPSILON__ 2.2204460492503131e-16L
#define __LDBL_DENORM_MIN__ 4.9406564584124654e-324L
#define __LDBL_HAS_INFINITY__ 1
#define __LDBL_HAS_QUIET_NAN__ 1
#define __REGISTER_PREFIX__ 
#define __USER_LABEL_PREFIX__ 
#define __VERSION__ "3.4.4 mipssde-6.03.00-20051020"
#define __OPTIMIZE_SIZE__ 1
#define __OPTIMIZE__ 1
#define __FINITE_MATH_ONLY__ 0

#define __mips__ 1
#define _mips 1
#define mips 1
#define __R3000 1
#define __R3000__ 1
#define R3000 1
#define _R3000 1
#define __mips_fpr 32
#define _MIPS_ARCH_MIPS32R2 1
#define _MIPS_ARCH "mips32r2"
#define _MIPS_TUNE_34KC 1
#define _MIPS_TUNE "34kc"
#define __mips 32
#define __mips_isa_rev 2
#define _MIPS_ISA _MIPS_ISA_MIPS32
#define __mips_soft_float 1
#define __MIPSEB 1
#define __MIPSEB__ 1
#define MIPSEB 1
#define _MIPSEB 1
#define __LANGUAGE_C 1
#define __LANGUAGE_C__ 1
#define LANGUAGE_C 1
#define _LANGUAGE_C 1
#define __gnu_linux__ 1
#define __linux 1
#define __linux__ 1
#define linux 1
#define __unix 1
#define __unix__ 1
#define unix 1



#define __PIC__ 1
#define __pic__ 1

#define _ABIO32 1
#define _MIPS_SIM _ABIO32
#define _MIPS_SZLONG 32
#define _MIPS_SZPTR 32
#define _MIPS_FPSET 16
#define _MIPS_SZINT 32
#define __ELF__ 1
# 1 "<command line>"
#undef __PIC__
#undef __pic__
#define __KERNEL__ 1
#define __UBOOT__ 1
#define CONFIG_SYS_TEXT_BASE 0x83F00000
#define CONFIG_MIPS 1
#define __MIPS__ 1
#define CONFIG_STANDALONE_UBOOT 1
# 1 "include/asm/arch/soc.h"

#define SOC_H 

#define ICACHE_SIZE 0x8000
#define DCACHE_SIZE 0x8000
#define CACHELINE_SIZE 32

#define SRAM_BASE 0x9F000000
#define SRAM_SIZE (96*1024)






#define FLASH_BASE 0x9FC00000


#define SPI_NOR_FLASH_MMIO_BASE0 (0x94000000)
#define SPI_NOR_FLASH_MMIO_SIZE0 (0x04000000)
#define SPI_NOR_FLASH_MMIO_BASE1 (0x9FC00000)
#define SPI_NOR_FLASH_MMIO_SIZE1 (0x00400000)


#define DECOMP_ALGO_LZMA 1
#define DECOMP_ALGO_GZIP 0

#define OTTO_NAND_FLASH 0
#define OTTO_NOR_SPI_FLASH 1


#define OTTO_DRAM_GEN 2
#define OTTO_PLL_GEN 2


#define OTTO_CPU_CYCLE() ({ int __res; __asm__ __volatile__("mfc0 %0, $9;" : "=r" (__res)); __res *= 2; __res; })
# 45 "include/asm/arch/soc.h"
#define OTTO_FLASH_ADDR_MODE() ({ int __res = (((*((volatile u32_t *)(0xB8001204)))>>9)&0x1)?4:3 ; __res; })






#define OTTO_FLASH_ENABLE_4BYTE_ADDR_MODE() ({ tmp = *((volatile u32_t *)(0xBB000418)); tmp = (tmp & 0xFFFFFFF7) | (0x8); *((volatile u32_t *)(0xBB000418)) = tmp; })






#define SYSTEM_RESET() do { *((volatile u32_t *)0xbb000014) = 0xffffffff; } while(0)




#define ON_FLASH_VMA_GAP 0xc00000
#define SOC_NUM_DRAM_SPARE 2
#define SOC_NUM_FLASH_SPARE 2
#define PLR_ENABLE_PLL_SET 
#define ZQ_TIMEOUT_RESET 1
# 79 "include/asm/arch/soc.h"
#define USE_SOC_SPARE 



#define ADDR_ALIGN(addr,algn) (((size_t)(addr)+((algn)-1))&(-(algn)))
#define CACHE_ALIGN(addr) ADDR_ALIGN(addr, CACHELINE_SIZE)

typedef unsigned int u32_t;
typedef unsigned short u16_t;
typedef unsigned char u8_t;
typedef int s32_t;
typedef short s16_t;
typedef char s8_t;


#define REG32(addr) (*((volatile u32_t *)(addr)))


#define NUM_WORD(bits) (((bits)+8*sizeof(u32_t)-1)/(8*sizeof(u32_t)))
#define VZERO ((void*)0)
#define SOC_ALIGN __attribute__((aligned(4)))

#define SOC_A0 0x0371
#define SOC_B0 0x6266
#define SYSTEM_STARTUP_CADDR 0x9FC00000
#define SYSTEM_STARTUP_UADDR 0xBFC00000
#define DRAM_CADDR 0x80000000
#define LPLR_DMA_SIZE 4096
#define NUM_LPLR 4
#define MAX_COPY_BLR 2
#define MAX_SPARESPACE 128
#define MAX_BLOCKS 2048
#define MAX_PLR_CHUNK 128
#define MAX_BLR_CHUNK 4096
#define MAX_CHUNK_SIZE 2048
#define SOC_CONF_OFFSET 32
#define SOC_HEADER_VERSION 0x00000924

#define UADDR(addr) ((size_t)(addr)|0x20000000)
#define CADDR(addr) ((size_t)(addr)&~0x20000000)

#define PADDR(addr) ((size_t)(addr)&~0xE0000000)


typedef struct {
    u16_t page_size;
    u16_t num_block;

    u16_t mtd_offset;
    u16_t bbi_dma_offset;

    u16_t bbi_raw_offset;
    u16_t bbi_swap_offset;

    u16_t num_page_per_block;
    u16_t num_pblr_block;



    u8_t enable_plr_hcheck;
    u8_t enable_plr_gc;
    u8_t cmd_set_id;
    u8_t addr_cycles;
    u8_t pblr_start_block;
    u8_t page_per_chunk;

    u8_t TRHZ;
    u8_t TDH;
} __attribute__((aligned(4))) nand_flash_info_t;

typedef struct {
    u8_t num_chips;
    u8_t addr_mode;
    u8_t prefer_divisor;
    u8_t size_per_chip;

    u8_t prefer_rx_delay0;
    u8_t prefer_rx_delay1;
    u8_t prefer_rx_delay2;
    u8_t prefer_rx_delay3;

    u8_t prefer_rd_cmd;
    u8_t prefer_rd_cmd_io;
    u8_t prefer_rd_dummy_c;
    u8_t prefer_rd_addr_io;
    u8_t prefer_rd_data_io;

    u8_t wr_cmd;
    u8_t wr_cmd_io;
    u8_t wr_dummy_c;
    u8_t wr_addr_io;
    u8_t wr_data_io;
    u8_t wr_boundary;


    u8_t erase_cmd;
    u8_t erase_unit;

    u8_t pm_method;
    u8_t pm_rdsr_cmd;
    u8_t pm_rdsr2_cmd;
    u8_t pm_wrsr_cmd;
    u8_t pm_enable_cmd;
    u16_t pm_enable_bits;
    u8_t pm_status_len;

    u8_t rdbusy_cmd;
    u8_t rdbusy_len;
    u8_t rdbusy_loc;
    u8_t rdbusy_polling_period;

    u8_t addr_4B_cmd;

    u32_t id;
} __attribute__((aligned(4))) nor_spi_flash_info_t;

#define NSPI_IO_SIO 0
#define NSPI_IO_DIO 1
#define NSPI_IO_QIO 2







    typedef nor_spi_flash_info_t flash_info_t;
#define RTK_MTD_DEV_NAME "rtk_spi_nor_mtd"
#define SECTION_ON_FLASH __attribute__ ((section (".flash_text")))
#define CONST_ON_FLASH __attribute__ ((section (".flash_data")))






typedef struct {
    u32_t bootloader1_addr;
    u32_t bootloader2_addr;
    u32_t kernel1_addr;
    u32_t kernel2_addr;
    u32_t rootfs1_addr;
    u32_t rootfs2_addr;
    u32_t env_addr;
    u32_t env_size;
    u32_t opt1_addr;
    u32_t opt2_addr;
    u32_t opt3_addr;
    u32_t opt4_addr;
    u32_t end_addr;
} __attribute__((aligned(4))) flash_layout_t;

typedef struct {
    u32_t dummy;
} __attribute__((aligned(4))) dram_gen1_info_t;

#define STATIC_CAL_DATA_NUM 33
typedef struct {

    u32_t mcr;
    u32_t dcr;
    u32_t dtr0;
    u32_t dtr1;
    u32_t dtr2;
    u32_t mpmr0;
    u32_t mpmr1;
    u32_t dider;
    u32_t d23oscr;
    u32_t daccr;
    u32_t dacspcr;
    u32_t dacspar;



    u32_t DDR1_mr;
    u32_t DDR1_emr;
    u32_t DDR2_mr;
    u32_t DDR2_emr1;
    u32_t DDR2_emr2;
    u32_t DDR2_emr3;
    u32_t DDR3_mr0;
    u32_t DDR3_mr1;
    u32_t DDR3_mr2;
    u32_t DDR3_mr3;


    u32_t static_cal_data_0;
    u32_t static_cal_data_1;
    u32_t static_cal_data_2;
    u32_t static_cal_data_3;
    u32_t static_cal_data_4;
    u32_t static_cal_data_5;
    u32_t static_cal_data_6;
    u32_t static_cal_data_7;
    u32_t static_cal_data_8;
    u32_t static_cal_data_9;
    u32_t static_cal_data_10;
    u32_t static_cal_data_11;
    u32_t static_cal_data_12;
    u32_t static_cal_data_13;
    u32_t static_cal_data_14;
    u32_t static_cal_data_15;
    u32_t static_cal_data_16;
    u32_t static_cal_data_17;
    u32_t static_cal_data_18;
    u32_t static_cal_data_19;
    u32_t static_cal_data_20;
    u32_t static_cal_data_21;
    u32_t static_cal_data_22;
    u32_t static_cal_data_23;
    u32_t static_cal_data_24;
    u32_t static_cal_data_25;
    u32_t static_cal_data_26;
    u32_t static_cal_data_27;
    u32_t static_cal_data_28;
    u32_t static_cal_data_29;
    u32_t static_cal_data_30;
    u32_t static_cal_data_31;
    u32_t static_cal_data_32;

    u32_t zq_setting;
    u8_t calibration_type;
    u8_t tx_clk_phs_delay;
    u8_t clkm_delay;
    u8_t clkm90_delay;
    u8_t auto_calibration;
    u8_t drv_strength;
} __attribute__((aligned(4))) dram_gen2_info_t;


#define DG2_REG_OFFSET_IPREF 23
#define DG2_REG_OFFSET_DPREF 22
#define DG2_REG_MASK_IPREF (0x1 << DG2_REG_OFFSET_IPREF)
#define DG2_REG_MASK_DPREF (0x1 << DG2_REG_OFFSET_DPREF)

#define DG2_REG_OFFSET_BANKCNT 28
#define DG2_REG_OFFSET_DBUSWID 24
#define DG2_REG_OFFSET_ROWCNT 20
#define DG2_REG_OFFSET_COLCNT 16
#define DG2_REG_OFFSET_DCHIPSEL 15
#define DG2_REG_OFFSET_FAST_RX 14
#define DG2_REG_OFFSET_BSTREF 13
#define DG2_REG_MASK_BANKCNT (0x3 << DG2_REG_OFFSET_BANKCNT )
#define DG2_REG_MASK_DBUSWID (0x3 << DG2_REG_OFFSET_DBUSWID )
#define DG2_REG_MASK_ROWCNT (0xF << DG2_REG_OFFSET_ROWCNT )
#define DG2_REG_MASK_COLCNT (0xF << DG2_REG_OFFSET_COLCNT )
#define DG2_REG_MASK_DCHIPSEL (0x1 << DG2_REG_OFFSET_DCHIPSEL)
#define DG2_REG_MASK_FAST_RX (0x1 << DG2_REG_OFFSET_FAST_RX )
#define DG2_REG_MASK_BSTREF (0x1 << DG2_REG_OFFSET_BSTREF )

#define DG2_REG_OFFSET_T_CAS 28
#define DG2_REG_OFFSET_T_WR 24
#define DG2_REG_OFFSET_T_CWL 20
#define DG2_REG_OFFSET_T_RTP 16
#define DG2_REG_OFFSET_T_WTR 12
#define DG2_REG_OFFSET_T_REFI 8
#define DG2_REG_OFFSET_T_REFI_UNIT 4
#define DG2_REG_MASK_T_CAS (0xF << DG2_REG_OFFSET_T_CAS )
#define DG2_REG_MASK_T_WR (0xF << DG2_REG_OFFSET_T_WR )
#define DG2_REG_MASK_T_CWL (0xF << DG2_REG_OFFSET_T_CWL )
#define DG2_REG_MASK_T_RTP (0xF << DG2_REG_OFFSET_T_RTP )
#define DG2_REG_MASK_T_WTR (0xF << DG2_REG_OFFSET_T_WTR )
#define DG2_REG_MASK_T_REFI (0xF << DG2_REG_OFFSET_T_REFI )
#define DG2_REG_MASK_T_REFI_UNIT (0xF << DG2_REG_OFFSET_T_REFI_UNIT)

#define DG2_REG_OFFSET_T_RP 24
#define DG2_REG_OFFSET_T_RCD 16
#define DG2_REG_OFFSET_T_RRD 8
#define DG2_REG_OFFSET_T_FAWG 0
#define DG2_REG_MASK_T_RP (0x1F << DG2_REG_OFFSET_T_RP )
#define DG2_REG_MASK_T_RCD (0x1F << DG2_REG_OFFSET_T_RCD )
#define DG2_REG_MASK_T_RRD (0x1F << DG2_REG_OFFSET_T_RRD )
#define DG2_REG_MASK_T_FAWG (0x1F << DG2_REG_OFFSET_T_FAWG)

#define DG2_REG_OFFSET_T_RFC 20
#define DG2_REG_OFFSET_T_RAS 12
#define DG2_REG_MASK_T_RFC (0xFF << DG2_REG_OFFSET_T_RFC)
#define DG2_REG_MASK_T_RAS (0x3F << DG2_REG_OFFSET_T_RAS)

#define DG2_REG_OFFSET_PM_MODE 28
#define DG2_REG_OFFSET_T_CKE 24
#define DG2_REG_OFFSET_T_RSD 12
#define DG2_REG_OFFSET_T_XSREF 0
#define DG2_REG_MASK_PM_MODE (0x3 << DG2_REG_OFFSET_PM_MODE)
#define DG2_REG_MASK_T_CKE (0xF << DG2_REG_OFFSET_T_CKE )
#define DG2_REG_MASK_T_RSD (0x3FF << DG2_REG_OFFSET_T_RSD )
#define DG2_REG_MASK_T_XSREF (0x3FF << DG2_REG_OFFSET_T_XSREF)

#define DG2_REG_OFFSET_T_XARD 28
#define DG2_REG_OFFSET_T_AXPD 24
#define DG2_REG_MASK_T_XARD (0xF << DG2_REG_OFFSET_T_XARD)
#define DG2_REG_MASK_T_AXPD (0xF << DG2_REG_OFFSET_T_AXPD)

#define DG2_REG_OFFSET_DQS0_EN_HCLK 31
#define DG2_REG_OFFSET_DQS0_EN_TAP 24
#define DG2_REG_OFFSET_DQS1_EN_HCLK 23
#define DG2_REG_OFFSET_DQS1_EN_TAP 16
#define DG2_REG_MASK_DQS0_EN_HCLK (0x1 << DG2_REG_OFFSET_DQS0_EN_HCLK)
#define DG2_REG_MASK_DQS0_EN_TAP (0x1F << DG2_REG_OFFSET_DQS0_EN_TAP )
#define DG2_REG_MASK_DQS1_EN_HCLK (0x1 << DG2_REG_OFFSET_DQS1_EN_HCLK)
#define DG2_REG_MASK_DQS1_EN_TAP (0x1F << DG2_REG_OFFSET_DQS1_EN_TAP )

#define DG2_REG_OFFSET_ODT_ALWAYS_ON 31
#define DG2_REG_OFFSET_TE_ALWAYS_ON 24
#define DG2_REG_MASK_ODT_ALWAYS_ON (0x1 << DG2_REG_OFFSET_ODT_ALWAYS_ON)
#define DG2_REG_MASK_TE_ALWAYS_ON (0x1 << DG2_REG_OFFSET_TE_ALWAYS_ON )

#define DG2_REG_OFFSET_AC_MODE 31
#define DG2_REG_OFFSET_DQS_SE 30
#define DG2_REG_OFFSET_DQS0_GROUP_TAP 16
#define DG2_REG_OFFSET_DQS1_GROUP_TAP 8
#define DG2_REG_OFFSET_AC_DYN_BPTR_CLR_EN 5
#define DG2_REG_OFFSET_AC_BPTR_CLEAR 4
#define DG2_REG_OFFSET_AC_DEBUG_SEL 0
#define DG2_REG_MASK_AC_MODE (0x1 << DG2_REG_OFFSET_AC_MODE )
#define DG2_REG_MASK_DQS_SE (0x1 << DG2_REG_OFFSET_DQS_SE )
#define DG2_REG_MASK_DQS0_GROUP_TAP (0x1F << DG2_REG_OFFSET_DQS0_GROUP_TAP )
#define DG2_REG_MASK_DQS1_GROUP_TAP (0x1F << DG2_REG_OFFSET_DQS1_GROUP_TAP )
#define DG2_REG_MASK_AC_DYN_BPTR_CLR_EN (0x1 << DG2_REG_OFFSET_AC_DYN_BPTR_CLR_EN )
#define DG2_REG_MASK_AC_BPTR_CLEAR (0x1 << DG2_REG_OFFSET_AC_BPTR_CLEAR )
#define DG2_REG_MASK_AC_DEBUG_SEL (0xF << DG2_REG_OFFSET_AC_DEBUG_SEL )

#define DG2_REG_OFFSET_AC_SILEN_PERIOD_EN 31
#define DG2_REG_OFFSET_AC_SILEN_TRIG 20
#define DG2_REG_OFFSET_AC_SILEN_PERIOD_UNIT 16
#define DG2_REG_OFFSET_AC_SILEN_PERIOD 8
#define DG2_REG_OFFSET_AC_SILEN_LEN 0
#define DG2_REG_MASK_AC_SILEN_PERIOD_EN (0x1 << DG2_REG_OFFSET_AC_SILEN_PERIOD_EN )
#define DG2_REG_MASK_AC_SILEN_TRIG (0x1 << DG2_REG_OFFSET_AC_SILEN_TRIG )
#define DG2_REG_MASK_AC_SILEN_PERIOD_UNIT (0xF << DG2_REG_OFFSET_AC_SILEN_PERIOD_UNIT)
#define DG2_REG_MASK_AC_SILEN_PERIOD (0xFF << DG2_REG_OFFSET_AC_SILEN_PERIOD )
#define DG2_REG_MASK_AC_SILEN_LEN (0xFF << DG2_REG_OFFSET_AC_SILEN_LEN )

#define DG2_REG_OFFSET_AC_SPS_DQ15R 31
#define DG2_REG_OFFSET_AC_SPS_DQ14R 30
#define DG2_REG_OFFSET_AC_SPS_DQ13R 29
#define DG2_REG_OFFSET_AC_SPS_DQ12R 28
#define DG2_REG_OFFSET_AC_SPS_DQ11R 27
#define DG2_REG_OFFSET_AC_SPS_DQ10R 26
#define DG2_REG_OFFSET_AC_SPS_DQ9R 25
#define DG2_REG_OFFSET_AC_SPS_DQ8R 24
#define DG2_REG_OFFSET_AC_SPS_DQ7R 23
#define DG2_REG_OFFSET_AC_SPS_DQ6R 22
#define DG2_REG_OFFSET_AC_SPS_DQ5R 21
#define DG2_REG_OFFSET_AC_SPS_DQ4R 20
#define DG2_REG_OFFSET_AC_SPS_DQ3R 19
#define DG2_REG_OFFSET_AC_SPS_DQ2R 18
#define DG2_REG_OFFSET_AC_SPS_DQ1R 17
#define DG2_REG_OFFSET_AC_SPS_DQ0R 16
#define DG2_REG_OFFSET_AC_SPS_DQ15F 15
#define DG2_REG_OFFSET_AC_SPS_DQ14F 14
#define DG2_REG_OFFSET_AC_SPS_DQ13F 13
#define DG2_REG_OFFSET_AC_SPS_DQ12F 12
#define DG2_REG_OFFSET_AC_SPS_DQ11F 11
#define DG2_REG_OFFSET_AC_SPS_DQ10F 10
#define DG2_REG_OFFSET_AC_SPS_DQ9F 9
#define DG2_REG_OFFSET_AC_SPS_DQ8F 8
#define DG2_REG_OFFSET_AC_SPS_DQ7F 7
#define DG2_REG_OFFSET_AC_SPS_DQ6F 6
#define DG2_REG_OFFSET_AC_SPS_DQ5F 5
#define DG2_REG_OFFSET_AC_SPS_DQ4F 4
#define DG2_REG_OFFSET_AC_SPS_DQ3F 3
#define DG2_REG_OFFSET_AC_SPS_DQ2F 2
#define DG2_REG_OFFSET_AC_SPS_DQ1F 1
#define DG2_REG_OFFSET_AC_SPS_DQ0F 0
#define DG2_REG_MASK_AC_SPS_DQ15R (0x1 << DG2_REG_OFFSET_AC_SPS_DQ15R)
#define DG2_REG_MASK_AC_SPS_DQ14R (0x1 << DG2_REG_OFFSET_AC_SPS_DQ14R)
#define DG2_REG_MASK_AC_SPS_DQ13R (0x1 << DG2_REG_OFFSET_AC_SPS_DQ13R)
#define DG2_REG_MASK_AC_SPS_DQ12R (0x1 << DG2_REG_OFFSET_AC_SPS_DQ12R)
#define DG2_REG_MASK_AC_SPS_DQ11R (0x1 << DG2_REG_OFFSET_AC_SPS_DQ11R)
#define DG2_REG_MASK_AC_SPS_DQ10R (0x1 << DG2_REG_OFFSET_AC_SPS_DQ10R)
#define DG2_REG_MASK_AC_SPS_DQ9R (0x1 << DG2_REG_OFFSET_AC_SPS_DQ9R )
#define DG2_REG_MASK_AC_SPS_DQ8R (0x1 << DG2_REG_OFFSET_AC_SPS_DQ8R )
#define DG2_REG_MASK_AC_SPS_DQ7R (0x1 << DG2_REG_OFFSET_AC_SPS_DQ7R )
#define DG2_REG_MASK_AC_SPS_DQ6R (0x1 << DG2_REG_OFFSET_AC_SPS_DQ6R )
#define DG2_REG_MASK_AC_SPS_DQ5R (0x1 << DG2_REG_OFFSET_AC_SPS_DQ5R )
#define DG2_REG_MASK_AC_SPS_DQ4R (0x1 << DG2_REG_OFFSET_AC_SPS_DQ4R )
#define DG2_REG_MASK_AC_SPS_DQ3R (0x1 << DG2_REG_OFFSET_AC_SPS_DQ3R )
#define DG2_REG_MASK_AC_SPS_DQ2R (0x1 << DG2_REG_OFFSET_AC_SPS_DQ2R )
#define DG2_REG_MASK_AC_SPS_DQ1R (0x1 << DG2_REG_OFFSET_AC_SPS_DQ1R )
#define DG2_REG_MASK_AC_SPS_DQ0R (0x1 << DG2_REG_OFFSET_AC_SPS_DQ0R )
#define DG2_REG_MASK_AC_SPS_DQ15F (0x1 << DG2_REG_OFFSET_AC_SPS_DQ15F)
#define DG2_REG_MASK_AC_SPS_DQ14F (0x1 << DG2_REG_OFFSET_AC_SPS_DQ14F)
#define DG2_REG_MASK_AC_SPS_DQ13F (0x1 << DG2_REG_OFFSET_AC_SPS_DQ13F)
#define DG2_REG_MASK_AC_SPS_DQ12F (0x1 << DG2_REG_OFFSET_AC_SPS_DQ12F)
#define DG2_REG_MASK_AC_SPS_DQ11F (0x1 << DG2_REG_OFFSET_AC_SPS_DQ11F)
#define DG2_REG_MASK_AC_SPS_DQ10F (0x1 << DG2_REG_OFFSET_AC_SPS_DQ10F)
#define DG2_REG_MASK_AC_SPS_DQ9F (0x1 << DG2_REG_OFFSET_AC_SPS_DQ9F )
#define DG2_REG_MASK_AC_SPS_DQ8F (0x1 << DG2_REG_OFFSET_AC_SPS_DQ8F )
#define DG2_REG_MASK_AC_SPS_DQ7F (0x1 << DG2_REG_OFFSET_AC_SPS_DQ7F )
#define DG2_REG_MASK_AC_SPS_DQ6F (0x1 << DG2_REG_OFFSET_AC_SPS_DQ6F )
#define DG2_REG_MASK_AC_SPS_DQ5F (0x1 << DG2_REG_OFFSET_AC_SPS_DQ5F )
#define DG2_REG_MASK_AC_SPS_DQ4F (0x1 << DG2_REG_OFFSET_AC_SPS_DQ4F )
#define DG2_REG_MASK_AC_SPS_DQ3F (0x1 << DG2_REG_OFFSET_AC_SPS_DQ3F )
#define DG2_REG_MASK_AC_SPS_DQ2F (0x1 << DG2_REG_OFFSET_AC_SPS_DQ2F )
#define DG2_REG_MASK_AC_SPS_DQ1F (0x1 << DG2_REG_OFFSET_AC_SPS_DQ1F )
#define DG2_REG_MASK_AC_SPS_DQ0F (0x1 << DG2_REG_OFFSET_AC_SPS_DQ0F )




    typedef dram_gen2_info_t dram_info_t;




typedef struct {
    u32_t set_by;
    u32_t sysclk_control_reg_25mhz;
    u32_t lx_pll_sel_reg_25mhz;
    u32_t sysclk_control_reg_40mhz;
    u32_t lx_pll_sel_reg_40mhz;
    u32_t mckg_phs_sel_reg;
    u32_t mckg_freq_div_reg;
} pll_gen1_info_t;

typedef struct {
    u32_t set_by;
    u32_t sys_cpu_pll_ctl0;
    u32_t sys_cpu_pll_ctl1;
    u32_t sys_mem_pll_ctl0;
    u32_t sys_mem_pll_ctl1;
    u32_t sys_lx_pll_ctl0;
    u32_t sys_lx_pll_ctl1;
} pll_gen2_info_t;

typedef struct {
    u32_t set_by;
    u32_t pll_cpu_ctl0;
    u32_t pll_cpu_ctl1;
    u32_t pll_cpu_misc_ctrl;
    u32_t pll_mem_ctl0;
    u32_t pll_mem_ctl1;
    u32_t pll_mem_misc_ctrl;
} pll_gen3_info_t;




    typedef pll_gen2_info_t pll_info_t;







typedef struct {

    u16_t baudrate_divisor;
} __attribute__((aligned(4))) peripheral_info_t;

typedef u8_t mac_addr_t[6];

typedef struct {
    mac_addr_t address;
} __attribute__((aligned(4))) mac_info_t;





    struct spare_header_s;
    typedef struct spare_header_s * spare_header_p;
    typedef void* void_p;



typedef struct spare_header_s {
    u32_t type;
    u32_t num_instances;
    void_p spare;
} spare_header_t;

#define SST_DRAM 0x4452414D
#define SST_FLASH 0x464C5348
#define SST_END 0x454E4420

typedef struct {
    u32_t header_type;
    u32_t header_ver;
    spare_header_p spare_headers;
    flash_info_t flash_info;
    flash_layout_t layout;
    dram_info_t dram_info;
    pll_info_t pll_info;
    peripheral_info_t peri_info;
    mac_info_t mac_info;
} __attribute__((aligned(4))) soc_t;




#define FIHT_SPI 0x00000001
#define FIHT_PARALLEL 0x00000002
#define FIHT_NAND 0x00000010
#define FIHT_NOR 0x00000020


#define FIHT_DRAMCTRL_MASK 0x00000f00
#define FIHT_DRAMCTRL_SHIFT 8
#define FIHT_DRAMCTRL_UKNOWN 0x00000000
#define FIHT_DRAMCTRL_GEN1 0x00000100
#define FIHT_DRAMCTRL_GEN2 0x00000200
#define FIHT_DRAMCTRL_GEN3 0x00000300
#define FIHT_DRAMCTRL_GEN4 0x00000400
#define FIHT_GET_DRAN_GEN(h) (((h)&FIHT_DRAMCTRL_MASK)>>FIHT_DRAMCTRL_SHIFT)


#define FIHT_PLL_MASK 0x0003f000
#define FIHT_PLL_SHIFT 12
#define FIHT_PLL_UNKNOWN 0x00000000
#define FIHT_PLL_GEN1 0x00001000
#define FIHT_PLL_GEN2 0x00002000
#define FIHT_PLL_GEN3 0x00003000
#define FIHT_PLL_GEN4 0x00004000
#define FIHT_PLL_GEN5 0x00005000
#define FIHT_PLL_GEN6 0x00006000
#define FIHT_PLL_GEN7 0x00007000
#define FIHT_GET_PLL_GEN(h) (((h)&FIHT_PLL_MASK)>>FIHT_PLL_SHIFT)


#define FIHT_FT_MASK 0xC0000000
#define FIHT_FT1 0x40000000
#define FIHT_FT2 0x80000000
#define FIHT_NORMAL 0x00000000

#define FIHT_IS_FT1(ht) (((ht)&FIHT_FT_MASK)==FIHT_FT1)
#define FIHT_IS_FT2(ht) (((ht)&FIHT_FT_MASK)==FIHT_FT2)
#define FIHT_IS_NORMAL(ht) (((ht)&FIHT_FT_MASK)==FIHT_NORMAL)
#define FIHT_DRAMCTRL_VER(ht) ((ht)&FIHT_DRAMCTRL_MASK)
#define FIHT_DRAMCTRL_IS_GEN1(ht) (FIHT_DRAMCTRL_VER(ht)==FIHT_DRAMCTRL_GEN1)
#define FIHT_DRAMCTRL_IS_GEN2(ht) (FIHT_DRAMCTRL_VER(ht)==FIHT_DRAMCTRL_GEN2)
#define FIHT_DRAMCTRL_IS_GEN3(ht) (FIHT_DRAMCTRL_VER(ht)==FIHT_DRAMCTRL_GEN3)
#define FIHT_DRAMCTRL_IS_GEN4(ht) (FIHT_DRAMCTRL_VER(ht)==FIHT_DRAMCTRL_GEN4)



typedef struct {
    u32_t signature1;
    const soc_t soc;
    u32_t signature2;
} soc_configuration_t;
#define FC_SIG1 0x03710601
#define FC_SIG2 0x62668696


typedef struct {
    u32_t signature;
    u16_t age;
    u16_t ver;
    u16_t chunk_id;
    u16_t num_chunk;
}oob_t ;


#define OOB_SIG_USED_PLR 0x505f4c52
#define OOB_SIG_USED_BLR 0x5f424c52
#define OOB_SIG_RESERVED 0x10400401
#define OOB_SIG_ERASED 0xffffffff
#define OOB_ERASED_AGE 0xffff

typedef union {
    u8_t u8_oob[128];
    oob_t oob;
} spare_u;

typedef struct {
    spare_u spare;
    u8_t chunk_buf[0];
} nand_dma_buf_t;


typedef enum {
    INI_RES_OK,
    INI_RES_UNKNOWN_MODE,
    INI_RES_UNKNOWN_MODEL,
    INI_RES_FAIL,
    INI_RES_TEST_FAIL,
    INI_RES_UNINIT,
    INI_RES_DRAM_ZQ_CALI_FAIL,
    INI_RES_DRAM_SW_CALI_FAIL
} init_result_t;

typedef enum {
 PLL_RES_OK,
 PLL_RES_BAD_MODE,
 PLL_RES_BAD_OSC_FREQ,
 PLL_RES_FREQ_OUT_OF_RANGE,
} pll_result_t;


#define RS_V2_PAGE_SIZE 512
#define RS_V2_OOB_SIZE 6
#define RS_V2_ECC_SIZE 10
#define RS_V2_UNIT_SIZE (RS_V2_PAGE_SIZE+RS_V2_OOB_SIZE+RS_V2_ECC_SIZE)


typedef int (nand_read_write_page_t)(u8_t *page_buf, spare_u *spare, u32_t page_id);
typedef u8_t (nand_read_bbi_t)(u32_t bytes, u32_t chunk_id );
typedef int (nand_erase_block_t)(u32_t chunk_id);
typedef int (nand_check_eccStatus_t) (void);
typedef int (nand_pio_write_t) (u32_t chunk_id,u32_t length, u8_t *buffer) ;

typedef u32_t (pblr_printf_t)(const char *, ...);
typedef void (fpv_u32_u32_t) (u32_t, u32_t);
typedef void (fpv_u32_t)(u32_t);
typedef u32_t (fpu32_u32_t)(u32_t);
typedef void (fpv_s32_t)(s32_t);
typedef void (fpv_s8_t)(s8_t);
typedef void (fpv_u32_s8_t)(u32_t,s8_t);
typedef s8_t (fps8_v)(void);
typedef s8_t (fps8_u32_t)(u32_t);
typedef s32_t (fps32_v)(void);
typedef s32_t (fps32_u32_t)(u32_t);
typedef s32_t (ps32_v)(u32_t,u8_t *);
typedef s32_t (fps32_u32_u32_u32_t) (u32_t, u32_t,u32_t);
typedef u32_t (fpu32_u32_u32_u32_t) (u32_t, u32_t,u32_t);

typedef int (nor_spi_read) (u32_t cid, u32_t offset, u32_t len, void *buf);
typedef int (nor_spi_write)(u32_t cid, u32_t offset, u32_t len, const void *buf);
typedef int (nor_spi_erase)(u32_t cid, u32_t offset);

typedef void* (memcpy_t)(u8_t *, const u8_t *, u32_t);
typedef void (void_func)(void);


typedef struct {
    const soc_t soc;


    u32_t soc_id;
    u32_t bond_id;
# 733 "include/asm/arch/soc.h"
        nor_spi_erase *_nor_spi_erase;
        nor_spi_read *_nor_spi_read;
        nor_spi_write *_nor_spi_write;



    init_result_t dram_init_result;
    init_result_t flash_init_result;


    pblr_printf_t *_pblr_printf;
    fpv_u32_t *_udelay;

    fpv_u32_u32_t *_uart_init;
    fpv_u32_s8_t *_uart_putc;
    fps8_u32_t *_uart_getc;
    fps32_u32_t *_uart_tstc;
    fps32_u32_u32_u32_t *_uart_ctrl;

    fps32_u32_u32_u32_t *dram_test;


    void_func *_dcache_writeback_invalidate_all;
    void_func *_icache_invalidate_all;


    fpu32_u32_t *_pll_query_freq;
} parameter_to_bootloader_t;



#define sram_parameters (*(parameter_to_bootloader_t*)(SRAM_BASE + SRAM_SIZE - CACHE_ALIGN(sizeof(parameter_to_bootloader_t))))


#define PLL_DEV_CPU 0
#define PLL_DEV_LX 1
#define PLL_DEV_MEM 2
#define PLL_DEV_DSP 3
#define PLL_MHZ_UNKNOWN 0xFFFFFFFF







#define UIH_COMP_NONE 0
#define UIH_COMP_GZIP 1
#define UIH_COMP_BZIP2 2
#define UIH_COMP_LZMA 3
#define UIH_COMP_LZO 4

typedef struct uimage_header {
    u32_t ih_magic;
    u32_t ih_hcrc;
    u32_t ih_time;
    u32_t ih_size;
    u32_t ih_load;
    u32_t ih_ep;
    u32_t ih_dcrc;
    u8_t ih_os;
    u8_t ih_arch;
    u8_t ih_type;
    u8_t ih_comp;
    u8_t ih_name[32];
} uimage_header_t;
